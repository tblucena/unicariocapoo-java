
public class Conta {
 
	private static int contador =0;
	
	//Atributos de inst�ncia
	 private int numero; 
	 private double saldo;
	 private boolean ativa;
	 
	 
	 //Construtor
	 
	 public Conta (double saldo) {
		numero = contador++;
		this.saldo = saldo;
		 
	 }
	 
	 
	 // Sobrecarga do construtor 
	 
	 public Conta (double saldo, boolean ativa) { 
	 
	 numero = contador++;
	 this.saldo = saldo;
	 this.ativa = ativa;
	 
	 }
	 
	 //M�todos de acesso p�blicos
	 
		public int getNumero() {
			return numero;
		}
		public void setNumero(int numero) {
			this.numero = numero;
		}
		public double getSaldo() {
			return saldo;
		}
		public void setSaldo(double saldo) {
			this.saldo = saldo;
		}
		
		
		// M�todos de neg�cio
		public void sacar(double num){
			this.saldo -= num;
		}
			
			public void depositar(double num){
				this.saldo += num;
		}
		
}
